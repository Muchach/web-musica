<?php  
//$c=new Contacto('nombre', 'correo@gmail.com', 30,"976213456");
//$agenda->agregar($c);

//$c=new Contacto('David', 'david@gmail.com', 40,"876524319");
//$agenda->agregar($c);

//$agenda->agregar(new Contacto('Sergio', 'sergio@gmail.com', 38,"650234158"));
//$agenda->agregar(new Contacto('Esther', 'esther@gmail.com', 38,"640218793"));
//$agenda->agregar(new Contacto('Pilar', 'pilar@gmail.com', 58,"123456789"));
require('classes/disco.class.php');
require('classes/coleccion.class.php');
//antes de nada comprobar que el usuario haya rellenado un nuevo contacto y si es así lo inserto en el fichero datos.txt
if(isset($_POST['enviar'])){
	//si se ha pulsado se reoge el resto de datos del formulario
	$titulo=$_POST['titulo'];
	$anyo=$_POST['anyo'];
	$grupo=$_POST['grupo'];

	//se crea la linea que se insertara en el archivo de texto
	$linea="\r\n".$titulo.';'.$anyo.';'.$grupo;
	//abro el fichero en modo escritura
	$fichero=fopen('datos.txt','a');
	//escribes la linea
	fwrite($fichero, $linea);
	//cierro el fichero
	fclose($fichero);
}

//Me creo un objeto de la clase Agenda
$disco=new Disco('Disco');

//se rellenaran los contacos desde un fichero de texto
//funciones para manjear ficheros de texto
$fichero=fopen('datos.txt','r');//modo leer
$linea=fgets($fichero);
//leer el fichero
while($linea=fgets($fichero)){
	$partes=explode(';',$linea);
	$titulo=$partes[0];
	$anyo=$partes[1];
	$grupo=$partes[2];
	$disco->agregar(new Coleccion($titulo, $anyo, $grupo));
}

//cerrar fichero
fclose($fichero);

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Mi agenda de contactos</title>
	<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet" media="screen">
</head>
<body>
	<section class="container">
		<header>
			<h1>Gestion de colección
				<small><?php echo $disco->titulo; ?></small>
			</h1>
	<table border="1" class="table table-hover table-striped">
		<tr>
			<th>Titulo</th>
			<th>Año</th>
			<th>Grupo</th>
			<th>Acciones</th>
		</tr>
		
		<?php foreach($disco->listar() as $disco){ ?>

			<tr>
				<td><?php echo $disco->titulo; ?></td>
				<td><?php echo $disco->anyo; ?></td>
				<td><?php echo $disco->grupo; ?></td>
				<td>Ver - Borrar - Modificar</td>
			</tr>

		<?php } ?>

	</table>

	<hr>

	<form action="index.php" method="POST">
		<input type="text" class="form-control" name="titulo" placeholder="Escribe el Titulo del disco">
		<input type="text" class="form-control" name="anyo" placeholder="Escribe el año del disco">
		<input type="text" class="form-control" name="grupo" placeholder="Escribe el grupo del disco">
		<button type="submit" name="enviar" value="enviar">Enviar<br>
	</form>
</section>
</body>
</html>