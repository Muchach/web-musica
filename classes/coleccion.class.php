<?php  

class Coleccion{

	public $titulo;
	public $anyo;
	public $grupo;

	public function __construct($titulo, $anyo, $grupo){
		$this->titulo=$titulo;
		$this->anyo=$anyo;
		$this->grupo=$grupo;
	}

	public function dimeInfo(){
		return $this->anyo . ', ' . $this->titulo . '('. $this->grupo .' años)';
	}

	

}
?>